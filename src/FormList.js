import React, {Component} from 'react';
import {connect} from 'react-redux';
import ArticleListView from './ArticleListView';

class FormList extends Component{
  render(){
    return(
      <div>
        <h1>All Topics</h1>
         {this.props.posts.map((post)=><ArticleListView key={post.id} post={post}/>)}
      </div>
    );
  }

}

const mapStateToProps = (state)=>{
  return{
    posts: state
  }
}

export default connect(mapStateToProps)(FormList);
