import React, {Component} from 'react';
import {connect} from 'react-redux';

class ArticleForm extends Component{

  handleSubmit = (e)=>{
    e.preventDefault();
    const title = this.getTitle.value;
    const message = this.getMessage.value;
    const data={
      id: new Date(),
      title,
      message
    }
    //console.log(data)

    this.props.dispatch({
      type:'ADD_POST',
      data
    });
    this.getTitle.value = '';
    this.getMessage.value = '';
  }

  render(){
      return(
        <div>
          <h1>Write a new Topic</h1>
          <form onSubmit={this.handleSubmit}>
            <input required type="text" ref={(input)=>this.getTitle = input} placeholder="New Topic"/><br/><br/>
            <textarea required rows="10" cols="50" ref={(input)=>this.getMessage = input} placeholder="Description"/><br/><br/>
            <button>Submit</button>
          </form>
        </div>
      );

  }

}

export default connect()(ArticleForm);
