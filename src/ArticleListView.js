import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactDOM from 'react-dom';

class ArticleListView extends Component{


  render(){
    return(
      <div className="TitleList" onClick={()=>RenderItem(this.props)}>
        <h2>{this.props.post.title}</h2>
      </div>
    );
  }

}

 function RenderItem(props){

  ReactDOM.render(<div>
    <h2>{props.post.title}</h2>
      <p className="Message" ><text>{props.post.message}</text></p>
      <button className="DeleteBtn" onClick={()=>destroy(props)}>
      Delete
      </button>
      </div>
    , document.getElementById("ArticleDetail"));
}

function destroy(props){
  ReactDOM.render(<div></div>, document.getElementById("ArticleDetail"));
  props.dispatch({
    type:'DELETE_POST',
    id: props.post.id
  });

}

//()=>console.log(this.props.post)
export default connect()(ArticleListView);
