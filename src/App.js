import React, { Component } from 'react';
import ArticleForm from './ArticleForm';
import FormList from './FormList';



class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="Head">
        <ArticleForm/>
        </div>
        <div className="Container">
        <div className="ListView">
        <FormList/>
        </div>
        <div id="ArticleDetail">

        </div>
        </div>
      </div>
    );
  }
}

export default App;
